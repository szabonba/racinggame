﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuHandler : MonoBehaviour {

    public GameObject PauseMenu;
    private Button ExitToMenuBtn;

    private void Start()
    {
        ExitToMenuBtn = PauseMenu.GetComponentInChildren<Button>();
        ExitToMenuBtn.onClick.AddListener(delegate { ExitToMenu(); });
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown("escape"))
        {
            if (PauseMenu.activeSelf)
            {
                HandlePauseMenu("hide");
            }
            else
            {
                HandlePauseMenu("show");
            }
            
        }
    }

    void HandlePauseMenu(string action)
    {
        if (action == "hide") //game continued
        {
            PauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
        else //game paused
        {
            PauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
        
    }

    void ExitToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
