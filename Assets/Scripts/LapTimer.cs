﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LapTimer : MonoBehaviour {

    public static double LapTime = 0.0;
    static Dictionary<int,double> LapTimes = new Dictionary<int, double>();
    double BestLap = 0.0;
    bool TimerOn = false;
    public int Lap = 0;
    int TotalLaps;
    bool passedCheckpoint = false;

    public Text TimeTxt;
    public Text BestTxt;
    public Text LastTxt;
    public Text LapNumberTxt;
    // Use this for initialization
    void Start() {
        LapTimes.Clear();
        if (TrackSelectHandler.GetTotalLaps() == 0)
        {
            TotalLaps = 1;
        }
        else
        {
            TotalLaps = TrackSelectHandler.GetTotalLaps();
        }
        
        LapNumberTxt.text = String.Format("Lap {0}/{1}", Lap, TotalLaps);
    }
	
	// Update is called once per frame
	void Update () {
        
        if (TimerOn)
        {
            LapTime += Time.deltaTime;
            TimeTxt.text = "Time: " + (Math.Round(LapTime, 2)).ToString();
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (TimerOn)
        {
            if (col.gameObject.name == "checkpoint")
            {
                passedCheckpoint = true;
            }
            if (col.gameObject.name == "end" && passedCheckpoint)
            {
                TimerOn = false;
                HandlePlayerPrefs(LapTime);
                if (LapTime < BestLap || Lap == 1)
                {
                    BestLap = LapTime;
                    BestTxt.text = "Best lap: " + (Math.Round(BestLap, 2)).ToString();
                    BestTxt.color = Color.green;
                    LastTxt.text = "Last lap: " + (Math.Round(LapTime, 2)).ToString();
                    LastTxt.color = Color.green;
                }
                else
                {
                    LastTxt.text = "Last lap: " + (Math.Round(LapTime, 2)).ToString();
                    LastTxt.color = Color.red;
                    BestTxt.color = Color.white;
                }
                LapTimes.Add(Lap, Math.Round(LapTime, 2));
                passedCheckpoint = false;
                LapTime = 0.0;
                GameObject.Find("start").GetComponent<Collider2D>().isTrigger = true;
                if (Lap == TotalLaps)
                {
                    SceneManager.LoadScene("Leaderboards");
                }
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (!TimerOn)
        {
            if (col.gameObject.name == "start")
            {
                TimerOn = true;
                Lap++;
                LapNumberTxt.text = String.Format("Lap {0}/{1}", Lap, TotalLaps);
            }
        }
    }

    void HandlePlayerPrefs(double LapTime)
    {
        string currentTrack = TrackSelectHandler.GetSelectedTrack();
        string currentCar = CarSelectHandler.GetSelectedCar();
        string key = String.Format("{0}-{1}-BestTime", currentCar, currentTrack); //"grn-Track2-BestTime"

        if (PlayerPrefs.GetFloat(key) == 0)
        {            
            PlayerPrefs.SetFloat(key, (float)LapTime);
            GhostRecorder.SaveGhost();
        }
        else
        {
            if ((float)LapTime < PlayerPrefs.GetFloat(key))
            {
                PlayerPrefs.SetFloat(key , (float)LapTime);
                GhostRecorder.SaveGhost();
            }
        }
    }

    public static Dictionary<int,double> GetLapTimes()
    {
        return LapTimes;
    }
}
