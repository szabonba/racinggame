﻿using Assets.Scripts.Menu;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TrackSelectHandler : MonoBehaviour {
    List<Button> ButtonsOnScene = new List<Button>();
    static int LapNumber;

    Button AddLapBtn, SubstractLapBtn;
    Text LapNumberTxt;

    static string SelectedTrackName;
    // Use this for initialization
    private void Awake()
    {
        //SelectedCarColor = color;
        ButtonsOnScene = GameObject.FindGameObjectsWithTag("btn").Select(item => item.GetComponent<Button>()).ToList();
        foreach (var item in ButtonsOnScene)
        {
            string trackName = item.name.Substring(0, 6);
            item.GetComponent<Button>().onClick.AddListener(delegate { SelectedTrack(trackName, item.gameObject.GetComponent<Button>()); });
        }
        
        
        AddLapBtn = GameObject.Find("AddLap").GetComponent<Button>();
        AddLapBtn.onClick.AddListener(delegate { LapNumberManager("add"); });
        SubstractLapBtn = GameObject.Find("SubstractLap").GetComponent<Button>();
        SubstractLapBtn.onClick.AddListener(delegate { LapNumberManager("substract"); });

        LapNumber = 1;
        LapNumberTxt = GameObject.Find("LapNumber").GetComponent<Text>();
        LapNumberTxt.text = LapNumber.ToString();
    }

    //track
    void SelectedTrack(string trackName, Button btn)
    {
        ColorBlock initialCb = btn.colors;
        ColorBlock cb = initialCb;
        cb.normalColor = Color.blue;

        SelectedTrackName = trackName;
        btn.colors = cb;
    }

    void LapNumberManager(string action)
    {
        if (action.Equals("add"))
        {
            LapNumber += 1;
            LapNumberTxt.text = LapNumber.ToString();
        }
        else
        {
            if (int.Parse(LapNumberTxt.text) > 1)
            {
                LapNumber -= 1;
                LapNumberTxt.text = LapNumber.ToString();
            }
        }

    }

    public static int GetTotalLaps()
    {
        return LapNumber;
    }

    public static string GetSelectedTrack()
    {
        return SelectedTrackName;
    }

    public void StartGame()
    {
        Debug.Log(string.Format("Car: {0} , track: {1} , laps: {2}", CarSelectHandler.GetSelectedCar(), SelectedTrackName, LapNumber.ToString()));
        LoadGame loadGame = new LoadGame(SelectedTrackName, CarSelectHandler.GetSelectedCar(), LapNumber);
        loadGame.StartGame();
    }
}
