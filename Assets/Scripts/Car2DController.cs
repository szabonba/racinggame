﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Car2DController : MonoBehaviour
{
    Rigidbody2D rb;
    string carType;

    float speedForce;
    float speedForceGrass;
    float speedForceNormal;
    float torqueForce;
    float driftFactorSticky;
    float driftFactorSlippy;
    float maxStickyVelocity = 10f;
    public static float carSpeed;
    float corneringForce;
    float cornerSlowdown = 0.98f;

    bool isOnDirt = false;
    bool isOnGrass = false;
    public Text SpeedTxt;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        carType = CarSelectHandler.GetSelectedCar();
        switch (carType)
        {
            case ("Grn"):
                speedForceNormal = 20f;
                speedForceGrass = speedForceNormal * 0.2f;
                torqueForce = 200f;
                driftFactorSticky = 0.25f;
                driftFactorSlippy = 0.999f;
                break;
            case ("Red"):
                speedForceNormal = 10f;
                speedForceGrass = speedForceNormal * 0.5f;
                torqueForce = 100f;
                driftFactorSticky = 0.25f;
                driftFactorSlippy = 0.999f;
                break;
            case ("Blu"):
                speedForceNormal = 10f;
                speedForceGrass = speedForceNormal * 0.5f;
                torqueForce = 100f;
                driftFactorSticky = 0.25f;
                driftFactorSlippy = 0.999f;
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        if (isOnDirt && isOnGrass)
        {
            driftFactorSticky = 0.999f;
            speedForce = speedForceGrass;
        }
        else if (isOnDirt)
        {
            driftFactorSticky = 0.95f;
            speedForce = speedForceNormal;
        }
        else if (isOnGrass)
        {
            driftFactorSticky = 0.999f;
            speedForce = speedForceGrass;
        }
        else
        {
            driftFactorSticky = 0.25f;
            speedForce = speedForceNormal;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateCarSpeed();
        corneringForce = Math.Abs(rb.angularVelocity);

        float driftFactor = driftFactorSticky;
        if (RightVelocity().magnitude > maxStickyVelocity)
        {
            driftFactor = driftFactorSlippy;
        }


        if (Input.GetButton("Accelerate"))
        {
            rb.AddForce(transform.up * speedForce);
        }

        if (Input.GetButton("Brake"))
        {
            rb.AddForce(-transform.up * speedForce);
        }

        if (carSpeed > 1)
        {
            rb.angularVelocity = (-1 * Input.GetAxis("Horizontal") * torqueForce);
        }
        else if (carSpeed < -1)
        {
            rb.angularVelocity = (1 * Input.GetAxis("Horizontal") * torqueForce);
        }
        else
        {
            rb.angularVelocity = (0 * Input.GetAxis("Horizontal"));
        }
        //rb.velocity = ForwardVelocity() + RightVelocity() * driftFactor;

        if (corneringForce > 99)
        {
            rb.velocity = ForwardVelocity() * cornerSlowdown + RightVelocity() * driftFactor;
        }
        else
        {
            rb.velocity = ForwardVelocity() + RightVelocity() * driftFactor;
        }

    }

    Vector2 ForwardVelocity()
    {
        return transform.up * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.up);
    }

    Vector2 RightVelocity()
    {
        return transform.right * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.right);
    }

    void UpdateCarSpeed()
    {
        carSpeed = transform.InverseTransformDirection(rb.velocity).y;
        SpeedTxt.text = "Speed: " + (Math.Round(carSpeed, 0) * 10).ToString();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "dirt")
        {
            isOnDirt = true;
        }

        if (col.name == "grass")
        {
            isOnGrass = true;
        }
    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.name == "dirt")
        {
            isOnDirt = false;
        }
        else if (col.name == "grass")
        {
            isOnGrass = false;
        }
    }
}
