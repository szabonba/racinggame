﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Menu
{
    class LoadGame
    {
        public static string Track;
        public static string Car;
        public static int Laps;

        public LoadGame(string T, string C, int L)
        {
            Track = T;
            Car = C;
            Laps = L;
        }

        public void StartGame()
        {
            SceneManager.LoadScene(Track);
        }
    }
}
