﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.GhostCar
{
    [System.Serializable]
    public class SerializableVector2
    {
        float x;
        float y;

        public SerializableVector2(float _x, float _y)
        {
            x = _x;
            y = _y;
        }

        public static List<SerializableVector2> Convert(List<Vector2> List)
        {
            List<SerializableVector2> ConvertedVector2 = new List<SerializableVector2>();

            foreach (var item in List)
            {
                float x = item.x;
                float y = item.y;
                ConvertedVector2.Add(new SerializableVector2(x, y));
            }

            return ConvertedVector2;
        }

        public static List<Vector2> ConvertToVector2(List<SerializableVector2> List)
        {
            List<Vector2> Vector2List = new List<Vector2>();

            foreach (var item in List)
            {
                float x = item.x;
                float y = item.y;
                Vector2List.Add(new Vector2(x, y));
            }

            return Vector2List;
        }
    }
}
