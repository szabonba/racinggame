﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts.GhostCar;

public static class SaveLoadGhost
{
    //public static Dictionary<string, double> BestLapsGhosts = new Dictionary<string, double>();
    public static List<Ghost> GhostData = new List<Ghost>();

    public static void Save(Ghost ghost)
    {
        GhostData.Add(ghost);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, SaveLoadGhost.GhostData);
        file.Close();
    }

    public static bool Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            SaveLoadGhost.GhostData = (List<Ghost>)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else
        {
            return false;
        }
    }
}