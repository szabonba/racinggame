﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DisplayLeaderboard : MonoBehaviour {
    Dictionary<int,double> LapTimes = LapTimer.GetLapTimes();
    Text TextBoard;
	
	void Start () {
        TextBoard = this.gameObject.GetComponent<Text>();
        TextBoard.text = "";
        foreach (var item in LapTimes)
        {
            TextBoard.text += string.Format("Lap {0}   {1}\n", item.Key, item.Value);
        }
        
	}

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
