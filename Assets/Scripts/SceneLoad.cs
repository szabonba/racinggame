﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoad : MonoBehaviour {
    GameObject StartCheckpoint;
    Dictionary<string, Sprite> Cars;
    

    private void Awake()
    {
        Cars  = new Dictionary<string, Sprite>()
        {
            {"Red", Resources.Load<Sprite>("Sprites/carRed")},
            {"Grn", Resources.Load<Sprite>("Sprites/carGreen")},
            {"Blu", Resources.Load<Sprite>("Sprites/carBlue")}
        };
    }

    void Start () {
        string currentTrack = TrackSelectHandler.GetSelectedTrack();
        string currentCar = CarSelectHandler.GetSelectedCar();
        this.GetComponent<SpriteRenderer>().sprite = Cars[currentCar];
        Debug.Log("Car: " + currentCar);
        Debug.Log("Track: " + currentTrack);
        Time.timeScale = 1;
        string key = String.Format("{0}-{1}-BestTime", currentCar, currentTrack);
        Debug.Log("Playerprefs: " + PlayerPrefs.GetFloat(key));
        StartCheckpoint = GameObject.Find("start");
        this.transform.position = StartCheckpoint.transform.position;
    }
}