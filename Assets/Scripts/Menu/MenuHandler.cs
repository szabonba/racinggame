﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Menu;

public class MenuHandler : MonoBehaviour
{
    public void ExitGame()
    {
        Application.Quit();
    }
}
