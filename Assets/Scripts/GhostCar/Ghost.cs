﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.GhostCar
{
    [System.Serializable]
    public class Ghost
    {
        public string Track { get; set; }
        public string Car { get; set; }
        public double LapTime { get; set; }
        public List<float> MagnitudeList = new List<float>();
        public List<SerializableVector2> CarPositionList { get; set; }
        
    }
}
