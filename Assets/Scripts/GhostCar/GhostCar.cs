﻿using Assets.Scripts.GhostCar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GhostCar : MonoBehaviour
{
    public GameObject Car;
    public float MoveSpeed = 2;
    private Vector2 Position;
    private int CurrentIndex = 1;
    static bool GhostLoaded = false;


    static Ghost FoundGhost;
    public List<SerializableVector2> SerializableVector2s;
    static List<Vector2> vector2s;

    Dictionary<string, Sprite> Cars;

    private void Awake()
    {
        Cars = new Dictionary<string, Sprite>()
        {
            {"Red", Resources.Load<Sprite>("Sprites/carRed")},
            {"Grn", Resources.Load<Sprite>("Sprites/carGreen")},
            {"Blu", Resources.Load<Sprite>("Sprites/carBlue")}
        };
    }

    void Start()
    {
        LoadGhost();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GhostLoaded && LapTimer.LapTime > 0.0)
        {
            MoveGhostCar();
        }
    }

    private Ghost LoadGhost(string track, string car)
    {
        SaveLoadGhost.Load();
        List<Ghost> GhostsList = SaveLoadGhost.GhostData;

        //find ghost at given track and car
        Ghost FoundGhost = GhostsList.Find(g => g.Car == car && g.Track == track);
        return FoundGhost;

        //vector2s = SerializableVector2.ConvertToVector2(FoundGhost.CarPositionList);
    }

    public void LoadGhost()
    {
        //find ghost at given track and car
        //FoundGhost = GhostsList.Find(g => g.Car == car && g.Track == track);
        if (SaveLoadGhost.Load())
        {
            List<Ghost> GhostsList = SaveLoadGhost.GhostData;
            string track = TrackSelectHandler.GetSelectedTrack();
            string car = CarSelectHandler.GetSelectedCar();

            if (GhostsList.Any(g => g.Car == car) && GhostsList.Any(g => g.Track == track))
            {
                FoundGhost = GhostsList.OrderBy(g => g.LapTime).FirstOrDefault(g => g.Car == car && g.Track == track);
                vector2s = SerializableVector2.ConvertToVector2(FoundGhost.CarPositionList);
                transform.position = vector2s[0];
                this.GetComponent<SpriteRenderer>().sprite = Cars[FoundGhost.Car];
                GhostLoaded = true;
            }
            else
            {
                Debug.Log("Brak ghosta dla tego auta");
                this.gameObject.SetActive(false);
            }

        }
        else
        {
            Debug.Log("brak pliku");
            this.gameObject.SetActive(false);
        }

    }

    private void MoveGhostCar()
    {
        while (Vector2.Distance(transform.position, vector2s[CurrentIndex]) < 0.5)
        {
            CurrentIndex++;
            CurrentIndex %= vector2s.Count;
        }

        transform.position = Vector3.MoveTowards(gameObject.transform.position, vector2s[CurrentIndex], FoundGhost.MagnitudeList[CurrentIndex] * Time.deltaTime);


        Vector2 dir = vector2s[CurrentIndex] - (Vector2)transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}