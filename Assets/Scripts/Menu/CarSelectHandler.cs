﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CarSelectHandler : MonoBehaviour {
    List<Button> ButtonsOnScene = new List<Button>();
    static string CarColor;

    // Use this for initialization
    void Awake () {
        ButtonsOnScene = GameObject.FindGameObjectsWithTag("btn").Select(item => item.GetComponent<Button>()).ToList();
        foreach (var item in ButtonsOnScene)
        {
            string carColor = item.name.Substring(item.name.Length - 3, 3);
            item.GetComponent<Button>().onClick.AddListener(delegate { SelectedCar(carColor); });
        }
    }

    private void SelectedCar(string carColor)
    {
        CarColor = carColor;
    }
    public static string GetSelectedCar()
    {
        return CarColor;
    }
	
}
