﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car2DDriftController : MonoBehaviour
{

    float speedForce = 10f;
    float torqueForce = 2f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (Input.GetButton("Accelerate"))
        {
            rb.AddForce(transform.up * speedForce);
        }

        if (Input.GetButton("Brake"))
        {
            rb.AddForce(-transform.up * speedForce);
        }

        rb.AddTorque(-1 * Input.GetAxis("Horizontal") * torqueForce);

    }
}
