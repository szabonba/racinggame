﻿using Assets.Scripts.GhostCar;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostRecorder : MonoBehaviour
{
    private Vector2 Position;
    
    static private List<Vector2> CarPositionList = new List<Vector2>();
    static private List<float> MagnitudeList = new List<float>();

    void Start()
    {
        Position = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if ((Math.Round(transform.position.x, 1) != Math.Round(Position.x, 1)) ||
            (Math.Round(transform.position.y, 1) != Math.Round(Position.y, 1)))
        {
            Position = transform.position;
            CarPositionList.Add(Position);
            MagnitudeList.Add(GetComponent<Rigidbody2D>().velocity.magnitude);
            Debug.Log(CarPositionList.Count);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "start")
        {
            CarPositionList.Clear();
            MagnitudeList.Clear();
        }
    }

    public static void SaveGhost()
    {
        Ghost Ghost = new Ghost
        {
            Track = TrackSelectHandler.GetSelectedTrack(),
            Car = CarSelectHandler.GetSelectedCar(),
            LapTime = LapTimer.LapTime,
            CarPositionList = SerializableVector2.Convert(CarPositionList),
            MagnitudeList = MagnitudeList
        };

        SaveLoadGhost.Save(Ghost);
    }
}