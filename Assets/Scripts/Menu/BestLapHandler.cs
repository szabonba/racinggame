﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class BestLapHandler : MonoBehaviour {
    public Text Rallycross, Oval, Short;

    //Track1
    public GameObject Track1Panel;
    Text[] Track1TxtArr;

    //Track2
    public GameObject Track2Panel;
    Text[] Track2TxtArr;

    //Track3
    public GameObject Track3Panel;
    Text[] Track3TxtArr;

    // Use this for initialization
    void Start () {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            Track1TxtArr = Track1Panel.GetComponentsInChildren<Text>();
            Track1TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track1-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track1TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track1-BestTime"), 2).ToString();
            Track1TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track1-BestTime"), 2).ToString();


            Track2TxtArr = Track2Panel.GetComponentsInChildren<Text>();
            Track2TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track2-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track2TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track2-BestTime"), 2).ToString();
            Track2TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track2-BestTime"), 2).ToString();

            Track3TxtArr = Track3Panel.GetComponentsInChildren<Text>();
            Track3TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track3-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track3TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track3-BestTime"), 2).ToString();
            Track3TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track3-BestTime"), 2).ToString();
        }
        else
        {
            PlayerPrefs.DeleteAll();
            Track1TxtArr = Track1Panel.GetComponentsInChildren<Text>();
            Track1TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track1-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track1TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track1-BestTime"), 2).ToString();
            Track1TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track1-BestTime"), 2).ToString();


            Track2TxtArr = Track2Panel.GetComponentsInChildren<Text>();
            Track2TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track2-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track2TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track2-BestTime"), 2).ToString();
            Track2TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track2-BestTime"), 2).ToString();

            Track3TxtArr = Track3Panel.GetComponentsInChildren<Text>();
            Track3TxtArr[0].text = "Best Red: " + Math.Round(PlayerPrefs.GetFloat("Red-Track3-BestTime"), 2).ToString();//"Grn-Track2-BestTime"        
            Track3TxtArr[1].text = "Best green: " + Math.Round(PlayerPrefs.GetFloat("Grn-Track3-BestTime"), 2).ToString();
            Track3TxtArr[2].text = "Best Blue: " + Math.Round(PlayerPrefs.GetFloat("Blu-Track3-BestTime"), 2).ToString();
        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
